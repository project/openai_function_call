<?php

namespace Drupal\openai_function_call;

/**
 * Defines the common interface for all Archiver classes.
 *
 * @see \Drupal\Core\Archiver\ArchiverManager
 * @see \Drupal\Core\Archiver\Annotation\Archiver
 * @see plugin_api
 */
interface OpenAIFunctionInterface {

  /**
   * Returns a list of dynamic function property definitions.
   *
   * @return array|null
   *   If null returned it will be converted to stdClass automatically.
   */
  public function getProperties(): ?array;

  /**
   * Function call
   */
  public function call($parameters);


}
