<?php

namespace Drupal\openai_function_call\Plugin\Openai\Function;

use Drupal\openai_function_call\OpenAIFunctionInterface;
use Drupal\openai_function_call\OpenAiFunctionPluginBase;

/**
 * The SetSiteName function definition.
 *
 * Sets the site name by the given parameter from OpenAI.
 *
 * @OpenAiFunction(
 *  id = "set_site_name",
 *  description = "Set the name of the drupal site",
 *  parameters = {
 *    "type" = "object",
 *    "properties" = {
 *      "name" = {
 *        "type" = "string",
 *        "description" = "The new name for the drupal site",
 *      }
 *    },
 *    "required" = {"name"},
 *  }
 * )
 */
class SetSiteName extends OpenAiFunctionPluginBase implements OpenAIFunctionInterface {

  public function call($parameters) {
    $name = $parameters['name'];
    if (!empty($name)) {

      $config = \Drupal::service('config.factory')->getEditable('system.site');
      $config->set('name', $name);
      $config->save();
      return $name;
    }


    return 'No parameters were provided';
  }

}
