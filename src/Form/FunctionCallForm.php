<?php

namespace Drupal\openai_function_call\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openai_function_call\FunctionPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to prompt OpenAI for answers.
 */
class FunctionCallForm extends FormBase {

  /**
   * The OpenAI client.
   *
   * @var \OpenAI\Client
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openai_function_call_form';
  }

  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->client = $container->get('openai.client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['prompt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter your prompt here. When submitted, OpenAI will generate a response. Please note that each query counts against your API usage for OpenAI.'),
      '#description' => $this->t('Based on the complexity of your prompt, OpenAI traffic, and other factors, a response can sometimes take up to 10-15 seconds to complete. Please allow the operation to finish.'),
      '#required' => TRUE,
    ];

    $functions = \Drupal::service('plugin.manager.openai_function_call');
    $plugin_definitions = $functions->getDefinitions();

    $options = [];

    foreach ($plugin_definitions as $plugin_id => $plugin_definition) {
      $options[$plugin_id] = $plugin_definition['description'];
    }

    $form['functions'] = [
      '#title' => $this->t('Functions'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => array_keys($options)
    ];


    $form['response'] = [
      '#type' => 'container',
      '#title' => $this->t('Response from OpenAI'),
      '#prefix' => '<div id="openai-prompt-response">',
      '#suffix' => '</div>',
    ];


    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Ask OpenAI'),
      '#ajax' => [
        'callback' => '::getResponse',
        'wrapper' => 'openai-prompt-response',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {


  }

  /**
   * {@inheritdoc}
   */
  public function getResponse(array &$form, FormStateInterface $form_state) {
    $prompt = $form_state->getValue('prompt');

    $functions = \Drupal::service('plugin.manager.openai_function_call');
    $plugin_definitions = $functions->getDefinitions();

    $tools = [];
    foreach ($plugin_definitions as $plugin_definition) {
      if (empty($plugin_definition['parameters']['properties'])) {
        $plugin_definition['parameters']['properties'] = new \stdClass();
      }

      $tools[] = [
        'type' => 'function',
        'function' => [
          'name' => $plugin_definition['id'],
          'description' => $plugin_definition['description'],
          'parameters' => $plugin_definition['parameters'],
        ],
      ];
    }

    $response = $this->client->chat()->create(
      [
        'model' => 'gpt-3.5-turbo-0613',
        'messages' => [
          ['role' => 'system', 'content' => 'You are a drupal developer now.s'],
          ['role' => 'user', 'content' => $prompt],
        ],
        'tools' => $tools,
      ]
    );

    $result = $response->toArray();

    if ($result['choices'][0]['finish_reason'] === 'tool_calls') {
      /** @var FunctionPluginManager $functionsManager */
      $functionsManager = \Drupal::service('plugin.manager.openai_function_call');

      foreach ($result['choices'][0]['message']['tool_calls'] as $tool_call) {

        $name = $tool_call['function']['name'];
        $arguments = $tool_call['function']['arguments'];

        $instance = $functionsManager->createInstance($name);
        $function_response = $instance->call(json_decode($arguments, true));
        $form['response']['#markup'] = $function_response;
      }

    }

    // Show the standard message response to the user.
    if (isset($result['choices'][0]['message']['content']) && $choice = $result['choices'][0]) {
      $form['response']['#markup'] = trim($choice['message']['content']) ?? $this->t('No answer was provided.');
    }

    return $form['response'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) { }

}
