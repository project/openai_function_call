<?php

namespace Drupal\openai_function_call\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a function annotation object.
 *
 * Plugin Namespace: Plugin\Openai\Function
 *
 * @see \Drupal\openai_function_call\FunctionPluginManager
 * @see \Drupal\openai_function_call\OpenAIFunctionInterface
 * @see plugin_api
 *
 * @Annotation
 */
class OpenAiFunction extends Plugin {

  /**
   * The function name.
   *
   * The plugin ID defined here will be used as function name when making API calls.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the archiver plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * The description of the function plugin passed to OpenAI.
   *
   * @ingroup plugin_translatable
   *
   * @var string
   */
  public $description;

  /**
   * An array containing function parameters. Can be overwritten with the getParameters function.
   *
   * @var array
   */
  public $parameters;

}
