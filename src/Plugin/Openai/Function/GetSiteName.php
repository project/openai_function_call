<?php

namespace Drupal\openai_function_call\Plugin\Openai\Function;

use Drupal\openai_function_call\OpenAiFunctionPluginBase;

/**
 * The GetSiteName plugin definition.
 *
 * Returns the site name from config when required.
 *
 * @OpenAiFunction(
 *  id = "get_site_name",
 *  description = "Get the name of the drupal site",
 *  parameters = {
 *    "type" = "object",
 *    "properties" = {}
 *  }
 * )
 */
class GetSiteName extends OpenAiFunctionPluginBase {

  public function call($parameters) {
    $config = \Drupal::config('system.site');
    return $config->get('name');
  }

}
